<?php
$page->template = false;
$permitido = array("jpeg","jpg","png","gif");
$file = UPLOADS_DIR.$_GET['i'];
$info = pathinfo($file);
if(!in_array($info['extension'],$permitido)){
	die("No permitido");	
}
header("Content-type: image/".$info['extension']);
$thumb = new thumb();
if(file_exists($file)){
	$thumb->loadImage($file);
	$thumb->resize($_GET['w'], 'width');
	$thumb->show();	
}
//$page->get_execution_time();
