<?php
include("thumb.class.php");

function imageUrl($image,$width = 300){
	return ABS_URL."img/$width/$image";	
}

global $router;
$router->redirect("thumb_img/(.*)/(.*)",PLUGINS_DIR.'thumbs/view.php?w=$1&i=$2');