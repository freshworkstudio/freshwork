<?php
class Model extends SQLQuery {
	protected $_model;
	function __construct() {
		global $conf,$inflect;
		$this->connect($conf["DB.HOST"],$conf["DB.USER"],$conf["DB.PASSWORD"],$conf["DB.NAME"]);
		$this->_development_env = $conf["APP.DEVELOPMENT_ENVIRONMENT"];
		$this->_model = get_class($this);
		if(!isset($this->_table))$this->_table = strtolower($inflect->pluralize($this->_model));
		if (!isset($this->abstract)) {
			$this->_describe();
		}
		$this->clear();
	}
	function clear(){
		global $inflect,$conf;
		parent::clear();
		$this->_limit = $conf["APP.PAGINATE_LIMIT"];
	}

	function __destruct() {
	}
	
	function paginador(){
			
	}
}
