<?php
//i'm a web page
class Page{
	static $instance = null;
	var $body,$head;
	var $title;
	var $file,$url,$rel_url,$complete_url,$query_string;
	var $ts_inicio,$ts_fin;
	var $rendered,$printed; 
	var $content; //Casi al finalizar todos los procesos, esta variable contiene todo el HTML que se imprimirá.
	var $standAlone; 
	var $javascripts = array();
	var $charset,$favicon;
	var $is_ajax; //Indica si la URL actual fué o no solicitada mediante AJAX.
	var $templates_directories,$template,$available_templates; //Especifíca que archivo que este dentro de la carpeta $template_dir se cargara como layout.
	var $components_directories;
	var $render_special_markup = true;
	var $hook,$component;
	var $put_additional_content = false;
	var $parse_html = true;
	function Page(){
		global $url;
		$this->file = $_SERVER['PHP_SELF'];
		//$this->rel_url = $_GET['url'];
		$this->url = $url;
		$this->title = $_SERVER['HTTP_HOST'];
		$this->rendered = false;
		$this->printed = false;
		$this->content = "";
		$this->standAlone = false;
		$this->ts_inicio = microtime(true);
		$this->is_ajax = $this->isAjax();
		$this->templates_directories = array(WWW_DIR."templates/");
		$this->components_directories = array(WWW_DIR."components/");
		$this->hook =& new Hook();
		$this->component =& new Component();
		//Remove url var from the get array and parrse as query
		$tmpget = $_GET; unset($tmpget["url"]);
		$this->query_string = $this->parseAsQuery($tmpget);
		
		$tmp = ($this->query_string != "")?$this->query_string:"?".$this->query_string;
		$this->complete_url = $this->url.$tmp;
		
		$this->refresh_templates();
		$this->template = (isset($this->available_templates["default"]))?"default":false;
		$this->put_additional_content = false;
	}
	
	
	
	
	
	function isAjax() {
		return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest') || isset($_POST['ajx']));
	}
	
	function parseAsQuery($arr){
		$tmp = "";
		foreach($arr as $key => $valor){
			if(is_array($valor)){
				$tmp = $this->parseAsQuery($valor);
			}else{
				$tmp = "$key=".urlencode($valor)."&";
			}
		}
		return substr($tmp,0,-1);
	}
	function getInstance(){
		if(self::$instance == null){
			self::$instance = new self();
		}
		return self::$instance;
	}
	function get_execution_time($echo = true){
		$this->ts_fin = microtime(true);
		$execution_time = $this->ts_fin - $this->ts_inicio;
		if($echo){
			echo "\n<!-- Execution time: {$execution_time}s -->";
		}
		return $execution_time;
	}
	function get($key){
		return $this->$key;
	}
	function set($key,$valor){
		$this->$key = $valor;
	}
	
	/********* TEMPLATES FUNCTIONS *****/
	function add_template_directory($dir){
		$this->templates_directories[] = $dir;
	}
	function add_components_directory($dir){
		$this->components_directories[] = $dir;
	}
	
	/* FUNCIONES GENERALES */
	//Javascript
	function import_js($str){
		$args = (is_array($str))?$str:func_get_args();
		foreach($args as $js){
			//$js = "/js/$js.js";
			$this->javascripts[] = $js; 
		}
		
	}
	
	//CSS
	function import_css($str){
		$args = (is_array($str))?$str:func_get_args();
		foreach($args as $css){
			//$css = "/css/$css.css";
			$this->css[] = $css; 
		}
		
	}
	//Title
	function put_title(){
		$title = $this->content->find("head title",0);
		if($title == NULL){
			$this->content->find("head",0)->innertext .= "\n\t<title>$this->title</title>"; 
		}else{
			$title->innertext = $this->title;	
		}
	}
	
	
	function put_js(){
		$head = $this->content->find("head",0);
		$salida = "\n\n\t<!-- Javascripts -->";
		foreach($this->javascripts as $js){
			$salida .= "\n\t<script type=\"text/javascript\" src=\"".Html::url($js)."\"></script>"; 	
		}
		$head->innertext .= $salida;
	}
	function put_css(){
		$head = $this->content->find("head",0);
		$salida = "\n\n\t<!-- CSS -->";
		foreach($this->css as $css){
			$salida .= "\n\t<link rel=\"stylesheet\" type=\"text/css\" href=\"".Html::url($css)."\" />"; 	
		}
		$salida .= "\n\t<!--CSS THEME --><link rel=\"stylesheet\" type=\"text/css\" href=\"".Html::url($this->css_theme)."\" /><!--CSS THEME -->"; 
		$head->innertext .= $salida;
	}
	function put_metatags(){
		$head = $this->content->find("head",0);
		$salida = "\n\n\t<!-- Metatags -->";
		$salida .= "\n\t".'<meta http-equiv="Content-Type" content="text/html; charset='.$this->charset.'" />';
		$salida .= "\n\t<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"".$this->favicon."\" />";
		$head->innertext .= $salida;
	}
	
	function put_head(){
		$head = $this->content->find("head",0);
		$head->innertext .= $this->head;	
	}
	/*** GETERS ***/
	function get_body(){
		return $this->body;	
	}
	function get_head(){
		return $this->head;	
	}
	
	//Setters
	function set_charset($str = "utf-8"){
		$this->charset = $str;
	}
	function refresh_templates(){
		$this->available_templates =  $this->get_templates();	
	}
	function get_templates(){
		/** GET available TEMPLATES */
		$available_templates = array();
		foreach($this->templates_directories as $dir){	
			if(file_exists($dir)){
				$res = opendir($dir);
				while ($element = readdir($res)){
					if( $element != "." && $element != ".."){
						if( is_file($dir.$element) ){
							$tmp = pathinfo($element);
							$available_templates[$tmp["filename"]] = $dir.$element ;
						} 
					}
				}
			}
		}
		return $available_templates;
	}
	
	function load(){
		ob_start();
		$this->rendered = true;
		$this->template = apply_filter("fw.set_template",$this->template);
		if($this->template == false){
			echo ($this->body);
		}else{
			if(!$this->is_ajax){
				//foreach($GLOBALS as $key => $valor)global $$key;
				global $page;
				
				
				
				if(isset($this->available_templates[$page->template])){
					require($this->available_templates[$page->template]);
				}else{
					throw new Exception('The requested template -'.$page->template."- doesn't exists");
					echo $this->body;	
				}
			}else{
				echo ($this->body);
			}
		}
		$this->content .= ob_get_contents();
		ob_clean();
		ob_end_flush();
		$this->content = apply_filter("after_render_content",$this->content);
	}
	function view(){
		if(!$this->rendered)$this->load();
		$this->printed = true;
		echo $this->content;
	}
	
	/** BUFFERING **/
	function start_buffering(){
		trigger("beforeBuffer");
		ob_start();	
	}
	function finish_head(){
		$head = ob_get_contents();
		$head = apply_filter("head_loads",$head);
		$this->set("head",$head);
		ob_clean();	
	}
	function end_buffering(){
		$body = ob_get_contents();
		$body = apply_filter("body_loads",$body);
		$this->set("body",$body);
		ob_clean();
		ob_end_flush();	
	}

	/* BOOOT FILES */
	function importBootFiles(){
		if(strpos($this->filename,WWW_DIR) === false)return;
		$rel = explode(DS,str_replace(WWW_DIR,"",$this->filename));
		
		
		unset($rel[count($rel)-1]);
		array_unshift($rel,"");
		$tmp = "";
		foreach($rel as $folder){
			$tmp .= $folder.DS;
			$f = substr(WWW_DIR,0,-1).$tmp."boot.php";
			if(file_exists($f)){
				include($f);	
			}
		}
	}
}
