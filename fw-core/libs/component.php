<?php
class Component{
	function load($tag,$arr = array()){
		global $page;
		Component::get($tag,$arr,false);
	}
	
	function get($tag,$arr = array(),$ob = true){
		foreach($arr as $key => $value)$$key = $value;
		$comps = Component::get_components();
		if(isset($comps[$tag])){
			if($ob)ob_start();
			include($comps[$tag]);
			if($ob)$res = ob_get_contents();
			else $res = true;
			if($ob)ob_end_clean();
			return $res;
		}else{
			throw new Exception('The requested component -'.$tag."- doesn't exists");	
		}
	}
	
	function get_components(){
		global $page;
		/** GET AVAIBLE COMPONENTS */
		$components = array();
		foreach($page->components_directories as $dir){	
			if(is_dir($dir)){
				$res = opendir($dir);
				while ($element = readdir($res)){
					if( $element != "." && $element != ".."){
						if( is_file($dir.$element) ){
							$tmp = pathinfo($element);
							$components[$tmp["filename"]] = $dir.$element ;
						} 
					}
				}
			}
		}
		return $components;
	}
}

function load_component($tag){
	$args = func_get_args();
	return call_user_func_array(array("Component","load"),$args);	
}
function get_component($tag){
	$args = func_get_args();
	return call_user_func_array(array("Component","get"),$args);	
}