<?php
$locale_dir = isset($GLOBALS["conf"]["LOCALE.DIR"])?$GLOBALS["conf"]["LOCALE.DIR"]:APP_DIR."locale".DS;
define("LOCALE_DIR",$locale_dir.DS);
class i18n{
	var $get = "lang";
	var $ses = "i18n";
	var $importado = false;
	var $default = "en";
	var $lang;
	var $cur_lang;
	var $lang_name;
	var $disponibles = array();
	static private $instancia = null;
	
	function i18n(){
		$this->getLocales();
		if(!$this->importado)$this->importLocaleFile();
	}
	
	function getLocales(){
		if(!is_dir(LOCALE_DIR)){
			//die("No se encuentra la carpeta ".LOCALE_DIR);	
		}else{
			$directorio= dir(LOCALE_DIR);
			while ($file = $directorio->read()){
				if($file != "." && $file != ".."){
					$this->disponibles[] = substr($file,0,-4);
				}
			}
			$directorio->close();
		}
	}
	
	function setLanguage($lang){
		$this->cur_lang = $lang;
		$_SESSION[$this->ses] = $lang;
	}
	function getInstance(){
		if(self::$instancia == null)self::$instancia = new self();
		return self::$instancia;
	}
	
	function getLang(){
		if($this->cur_lang){
			$idioma = $this->cur_lang;
		}else{
			if(isset($_GET[$this->get])){
				$idioma = $_GET[$this->get];
			}else{
				if(isset($_SESSION[$this->ses])){
					$idioma = $_SESSION[$this->ses];	
				}else{
					$idioma = $this->getDefaultLanguage();
					
				}
			}
		}
		$this->cur_lang = $idioma;
	}
	function importLocaleFile($lang = false){
		if($lang !== false)$this->cur_lang = $lang;
		
		$this->getLang();
		$file = $this->getLocaleFile($this->cur_lang);
		if($file != false){
			include($file);
			$_SESSION[$this->ses] = $this->cur_lang;
			$this->lang_name = $lang_name;
			$this->lang = $lang;
		}else{
			trigger_error ( "No se encuentra archivo de idioma para '$this->cur_lang'" );//die("No se encuentra archivo de idioma para '$this->cur_lang'");
		}
	}
	
	function getLocaleFile($lang){
		$file = LOCALE_DIR.$lang.".php";
		if(!file_exists($file)){
				
		}else{
			return $file;
		}
		return false;
		
	}
	
	function translate($str){
		$args = (is_array($str))?$str:func_get_args();
		$args[0] = html_entity_decode($args[0]);
		$args[0] = isset($this->lang[$args[0]])?$this->lang[$args[0]]:$args[0];
		return (call_user_func_array('sprintf',$args));
	}
	
	function getLink($lang){
		$link = $_SERVER['REQUEST_URI'];
		$part = explode("?",$link);
		if(isset($part[1])){
			$link = $part[0];	
			$gets = explode("=",$part[1]);
			//FALTA POR TERMINAR....
		}
		return $link;
	}
	#########################################################
	# Copyright © 2008 Darrin Yeager                        #
	# http://www.dyeager.org/                               #
	# Licensed under BSD license.                           #
	#   http://www.dyeager.org/downloads/license-bsd.php    #
	#########################################################
	
	function getDefaultLanguage() {
	   if (isset($_SERVER["HTTP_ACCEPT_LANGUAGE"]))
		  return $this->parseDefaultLanguage($_SERVER["HTTP_ACCEPT_LANGUAGE"]);
	   else
		  return $this->parseDefaultLanguage(NULL);
	   }
	
	function parseDefaultLanguage($http_accept) {
	   	if(isset($http_accept) && strlen($http_accept) > 1)  {
		  # Split possible languages into array
		  $x = explode(",",$http_accept);
		  foreach ($x as $val) {
			 #check for q-value and create associative array. No q-value means 1 by rule
			 if(preg_match("/(.*);q=([0-1]{0,1}\.\d{0,4})/i",$val,$matches))
				$lang[$matches[1]] = (float)$matches[2];
			 else
				$lang[$val] = 1.0;
		  }
		  
		  asort($lang,SORT_NUMERIC );
		  $lang = array_reverse($lang);
		  
		  foreach($lang as $key => $valor){
			if(in_array($key,$this->disponibles))return strtolower($key);
			if(in_array(substr($key,0,2),$this->disponibles))return strtolower(substr($key,0,2));
		  }
	   }
	   return strtolower($this->default);
	}
}

//HERRAMIENTAS DE IDIOMA
function ___($str){
	global $i18n;
	$args = (is_array($str))?$str:func_get_args();
	echo __($args); 
}
function __($str){
	global $i18n;
	$i18n = $i18n->getInstance();
	$args = (is_array($str))?$str:func_get_args();
	return ($i18n->translate($args));
}
