<?php
class Router{
	var $routing = array();
	var $valid_extensions = array("php","php5","html","htm");
	
	function redirect($pattern,$result,$template = null){
		$this->routing[$pattern] = $result;
	}
	function getRedirect($url){
		
		$preg = false;
		foreach($this->routing as $origen => $destino){
			$replac = "/^".str_replace("/","\/",$origen)."$/";
			if(preg_match($replac,$url)){
				$preg = true;
				$file = preg_replace("/".str_replace("/","\/",$origen)."/",$destino,$url);
				//if($file{0} != "/")$file = WWW_DIR.$file;
				//if(!is_file($file))return false;
				return $file;
			}
		}
		return $url;
	}
	function routeURL($url){
		
		$redirect = $this->getRedirect($url);
		//if($url{0} == "/")$url = substr($url,1);
		if(!isset($redirect{0}) || $redirect{0} != "/")$redirect = $this->getFile($redirect);
		
		if(strpos($redirect,"?") !== false){
			$redirect = explode("?",$redirect);
			$q = $redirect[1];
			$redirect = $redirect[0];
			parse_str($q,$_GET);
		}
		
		if(!is_file($redirect))return false;
		return $redirect;
	}
	
	//@param string url: Relative URL coming from http request
	function getFilename($url){
		$filename = $this->getRedireccion($url);
		if(!$filename){
				
		}
	}
	
	function getFile($url){
		if($url == "")$url = "index";
		if(strrchr($url,"/")== "/")$url .= "index";
		$path = pathinfo($url);
		$ext = (isset($path["extension"]))?$path["extension"]:"";	
		if(in_array($ext,$this->valid_extensions)){
			$file = WWW_DIR.$url;
			if(!is_file($file))return false;
		}else{
			$file = WWW_DIR.$url;
			foreach($this->valid_extensions as $ext_valida){
				$request_file =  WWW_DIR.$url.".$ext_valida";
				if(file_exists($request_file)){
					$file = $request_file;
					break;	
				}
			}
			if(is_dir($file)){
				$file = $this->routeURL($url."/");
				if(is_file($file)){
					header("Location: ".$url."/");
					exit;	
				}
			}
			if(!is_file($file))return false;
			
		}
		return $file;
	}
}?>