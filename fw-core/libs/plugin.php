<?php

class Plugin{
	var $name;
	
	
	static function getList(){
		global $conf,$cache;
		$plugins = array();
		if($conf["APP.DEVELOPMENT_ENVIRONMENT"] || $cache->get('fw-plugins') == NULL){
			$plugins = self::refreshPluginsList();
			$cache->set('fw-plugins',$plugins);
		}else{
			$plugins = $cache->get('fw-plugins');
		}
		return $plugins;		
	}
	static function refreshPluginsList(){
		$plugins = array();
		if ($gestor = opendir(PLUGINS_DIR)) {
		 
			/* Esta es la forma correcta de iterar sobre el directorio. */
			while (false !== ($entrada = readdir($gestor))) {
				if($entrada != ".." && $entrada != "." && substr($entrada,0,1) != "_"){
					$plugins[] = $entrada;
				}
			}
		 
			closedir($gestor);
		}
		return $plugins;
	}
	static function init(){
		$plugins = self::getList();
		
		
		foreach($plugins as $plugin){
			$f = PLUGINS_DIR.$plugin.DS."boot.php";
			if(file_exists($f))include($f);
		}
	}
}