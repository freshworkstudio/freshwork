<?php


/** Check for Magic Quotes and remove them **/

function stripSlashesDeep($value) {
	$value = is_array($value) ? array_map('stripSlashesDeep', $value) : stripslashes($value);
	return $value;
}

function removeMagicQuotes() {
if ( get_magic_quotes_gpc() ) {
	$_GET    = stripSlashesDeep($_GET   );
	$_POST   = stripSlashesDeep($_POST  );
	$_COOKIE = stripSlashesDeep($_COOKIE);
}
}

/** Check register globals and remove them **/

function unregisterGlobals() {
    if (ini_get('register_globals')) {
        $array = array('_SESSION', '_POST', '_GET', '_COOKIE', '_REQUEST', '_SERVER', '_ENV', '_FILES');
        foreach ($array as $value) {
            foreach ($GLOBALS[$value] as $key => $var) {
                if ($var === $GLOBALS[$key]) {
                    unset($GLOBALS[$key]);
                }
            }
        }
    }
}

/** Main Call Function **/


/** Autoload any classes that are required **/


function __autoload($className) {
	if (file_exists(CORE_LIBS_DIR. strtolower($className) . '.php')) {
		require_once(CORE_LIBS_DIR. strtolower($className) . '.php');
	} elseif(file_exists(MODELS_DIR. strtolower($className) . '.php')) {
		require_once(MODELS_DIR. strtolower($className) . '.php');
		/* Error Generation Code Here */
	}
}

removeMagicQuotes();
unregisterGlobals();
