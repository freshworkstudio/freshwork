<?php

/*** Main Includes Begins **/
$page->start_buffering();
trigger("fw.start_buffering");

$auto_controller_file = $pathinfo["dirname"]."/".$pathinfo["filename"].".cont.".$pathinfo["extension"];
if(file_exists($auto_controller_file)){
	include($auto_controller_file);	
	trigger("fw.controller_include");
}

include($page->filename);

trigger("fw.page_include");
$page->end_buffering();
//$page->head and $page->body its filled at this time 
trigger("fw.end_buffering");

$page->view();

global $conf;
if($conf["APP.DEVELOPMENT_ENVIRONMENT"] && !$page->isAjax() && $page->put_additional_content)$page->get_execution_time(); 