<?php

/** CONFIGURATIONS **/
require_once (CORE_DIR . 'base-config.php');
require_once (CONFIG_DIR . 'config.php');
require_once (CORE_DIR. 'shared.php');


/*** CHECK IF IN DEVELOPMENT ENVIROMENT */
if ($conf["APP.DEVELOPMENT_ENVIRONMENT"] == true) {
	error_reporting(E_ALL);
	ini_set('display_errors','On');
} else {
	error_reporting(E_ALL);
	ini_set('display_errors','Off');
	ini_set('log_errors', 'On');
}
ini_set('error_log', LOGS_DIR.'error.log');
$page =& new Page();
$router =& new Router();
$cache =& new Cache();
$inflect =& new Inflection();
$i18n =& new i18n();

trigger("fw.start_loading");


require_once (CONFIG_DIR . 'routing.php');
Plugin::init();

trigger("fw.after_plugins_init");

//Url 
$page->url = $url;
$page->filename = $router->routeURL($url);

//$page->filename = $router->getFile($page->url);
$page->current_directory = dirname($page->filename).DS;
if($page->filename === false){
	header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found"); 
	header("Status: 404 Not Found");
	exit;	
}

$pathinfo = pathinfo($page->filename);
if(!in_array($pathinfo["extension"],$router->valid_extensions)){
	$url = str_replace(ABS_DIR,"",$page->filename);
	header($_SERVER["SERVER_PROTOCOL"]." 301 Moved Permanently");
	header("Location: ".ABS_URL.$url,301);
}


$page->importBootFiles();
trigger("fw.after_boots_load");


require_once (CORE_DIR."main.php");