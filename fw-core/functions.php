<?php
function get_by_id($model,$id){
	$model = ucwords($model);
	$tmp = new $model;
	$tmp->where($tmp->get_pk(),$id);
	return $tmp->search(true);	
}

/*
TODO: Remove this functions from Core
*/
function validate_data($data,$validaciones){
	if(!is_array($data))return false;
	$errors = array();
	foreach($validaciones as $field => $vals){
		if(is_string($vals))$vals = explode(",",$vals);
		
		$value = (isset($data[$field]))?$data[$field]:false;
		foreach($vals as $validation => $values){
			if(!is_array($values)){$validation = $values;$values = array(); }
			switch($validation){
				case 'mandatory':
					$errstr = (isset($values['msg']))?$values['msg']:"Este campo es obligatorio";
					if(is_string($value) && !trim($value))$errors[$field][] = $errstr;
					break;	
				case "only_numbers":
					$errstr = (isset($values['msg']))?$values['msg']:"Solo puedes ingresar números";
					if(!ctype_digit($value))$errors[$field][] = $errstr;
					break;
				case "rut":
					$errstr = (isset($values['msg']))?$values['msg']:"R.U.T. inválido";
					if(!rut_valido($value))$errors[$field][] = $errstr;
					break;
				case "email":
					$errstr = (isset($values['msg']))?$values['msg']:"El email ingresado es inválido";
					if(!valida_email($value))$errors[$field][] = $errstr;
					break;
				case "length":
					$max = (isset($values['max']))?$values['max']:false;
					$min = (isset($values['min']))?$values['min']:false;
					if($min || $max){
						$def = "";
						if($min == $max){
							$def = "El valor ingresado debe ser igual a $max";
						}else{
							$def = "El valor ingresado no puede ser ";
							if($min)$def .= "menor a $min";
							if($min && $max) $def .= " ni ";
							if($max)$def .= "mayor a $max";
							$def .= " caracteres";
						}
						$errstr = (isset($values['msg']))?$values['msg']:$def;	
						if(($min && strlen($value) < $min) || ($max && strlen($value) > $max)){
							$errors[$field][] = $errstr;
						}
						
					}
					break;
			}
		}
	}
	return $errors;
}
function rut_valido( $rut ){
	if( empty( $rut ) ) return false;
	$rut = normalizarut($rut);
	if (!preg_match("/(\d{7,8})([\dK])/", strtoupper($rut), $aMatch)) { 
	 return false; 
	} 
	$sRutBase = substr(strrev($aMatch[1]) , 0, 8 ); 
	$sCodigoVerificador = $aMatch[2]; 
	$iCont = 2; 
	$iSuma = 0; 
	for ($i = 0;$i<strlen($sRutBase);$i++) { 
	 if ($iCont>7) { 
		 $iCont = 2; 
	 } 
	 $iSuma+= ($sRutBase{$i}) *$iCont; 
	 $iCont++; 
	} 
	$iDigito = 11-($iSuma%11); 
	$sCaracter = substr("-123456789K0", $iDigito, 1); 
	return ($sCaracter == $sCodigoVerificador);
} 
function valida_email($email){ 
   	$mail_correcto = 0; 
   	//compruebo unas cosas primeras 
   	if ((strlen($email) >= 6) && (substr_count($email,"@") == 1) && (substr($email,0,1) != "@") && (substr($email,strlen($email)-1,1) != "@")){ 
      	 if ((!strstr($email,"'")) && (!strstr($email,"\"")) && (!strstr($email,"\\")) && (!strstr($email,"\$")) && (!strstr($email," "))) { 
         	 //miro si tiene caracter . 
         	 if (substr_count($email,".")>= 1){ 
            	 //obtengo la terminacion del dominio 
            	 $term_dom = substr(strrchr ($email, '.'),1); 
            	 //compruebo que la terminación del dominio sea correcta 
            	 if (strlen($term_dom)>1 && strlen($term_dom)<5 && (!strstr($term_dom,"@")) ){ 
               	 //compruebo que lo de antes del dominio sea correcto 
               	 $antes_dom = substr($email,0,strlen($email) - strlen($term_dom) - 1); 
               	 $caracter_ult = substr($antes_dom,strlen($antes_dom)-1,1); 
               	 if ($caracter_ult != "@" && $caracter_ult != "."){ 
                  	 $mail_correcto = 1; 
               	 } 
            	 } 
         	 } 
      	 } 
   	} 
   	if ($mail_correcto) 
      	 return 1; 
   	else 
      	 return 0; 
}

function normalizarut($rut){
	$tmp = str_replace(array("-",",","."),"",$rut);
	return $tmp;	
}