<?php

/* DB CONFIG */
$conf["DB.TYPE"] 		= "mysql";
$conf["DB.HOST"] 		= "localhost";
$conf["DB.NAME"]		= "database_name";
$conf["DB.USER"] 		= "username";
$conf["DB.PASSWORD"]	= "password";

/* APP CONFIG */	
$conf["APP.PAGINATE_LIMIT"] 			= 100;
$conf["APP.DEVELOPMENT_ENVIRONMENT"]	= false;
